source("lib/folder-setup.r")
library(svglite)

3+2
3+2*4

data(PlantGrowth)
print(PlantGrowth)

print("Mean weight of all plants")
print(mean(PlantGrowth$weight))

print("Standard deviation of plant weight")
print(sd(PlantGrowth$weight))

print("Mean weight for each group")
print(aggregate(weight ~ group, data = PlantGrowth, mean))

if (dir.exists("output") == TRUE) {
	unlink("output",recursive=TRUE)
}
dir.create("output")

# Create a histogram
svglite(file="output/PlantGrowth-weight-histogram.svg")
hist(PlantGrowth$weight,main="Histogram of plant dry weight",xlab="dry weight (g)")
dev.off()

# Create a boxplot
svglite(file="output/PlantGrowth-group-weight-boxplot.svg")
boxplot(weight ~ group, data=PlantGrowth,main="Box-plot of plant dry weight by treatment condition",ylab="dry weight (g)",xlab="treatment condition")
dev.off()
