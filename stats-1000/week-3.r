source("lib/folder-setup.r")
library(svglite)
library(readxl)

# Load the titanic dataset
titanic <- read_excel("data/titanic.xlsx")

print("Table of titanic survivors")
print(titanic)

# Create a table from the titanic dataset
titanic_tab  <- table(titanic$survived, titanic$pclass)
print(titanic_tab)

print("addmargins in its default state will create a sum of rows and columns")
print(addmargins(titanic_tab))

titanic_prop  <- prop.table(titanic_tab, margin = 2)
print(titanic_prop)

# A stacked bar chart of titanic survivors based on passenger class with a legend on the top left
svglite(file="output/titanic_tab-barplot-stacked")
barplot(titanic_tab,
		legend.text = TRUE,
		args.legend = list(x = "topleft")
		)
dev.off()

# A side-by-side bar chart of the above
svglite(file="output/titanic_tab-barplot-side-by-side.svg")
barplot(titanic_tab,
		beside = TRUE,
		legend.text = TRUE,
		args.legend = list(x = "topleft")
		)
dev.off()

# A stacked bar chart of the proportion of titanic survivors based on passenger class with a legend on the top left
svglite(file="output/titanic_prop-barplot-side-by-side.svg")
barplot(titanic_prop,
		beside = TRUE,
		legend.text = TRUE,
		args.legend = list(x = "topleft")
		)
dev.off()

# Load the bone dataset
osteo  <- read_excel("data/bone.xlsx")
print("Osteo dataset")
print(osteo)
# IV = whether or not participants play baseball
# DV = Bone strength (cm^4/1000)

# Scatterplot of bone strength by hand
svglite(file="output/osteo-plot.svg")
plot(NonDom ~ Dom, data = osteo)
dev.off()
# Positive relationship, linear, medium, a couple outliers

# Linear regression of bone dataset
osteo_lm  <- lm(NonDom ~ Dom, data = osteo)
print(summary(osteo_lm))
# b_0 ~ 7.331
# b_1 ~ 0.448

#y=b_0+b_1x
#y=7.331+0.448x
#y=7.331+0.448*20
#y=16.291

# Given a dominant bone strength of 20cm^4/1000, non-dominant bone strength is predicted to be 16.291cm^4/1000
