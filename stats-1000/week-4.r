source("lib/folder-setup.r")
library(svglite)
library(readxl)

# Allocate 100 subjects into two groups of 50 subjects each
two_treatments <- c("A","B") #Create a vector
twotreat_subjects  <- data.frame(
						IDs = 1:100, #IDs 1 to 100
						treatment = rep(two_treatments, each = 50) #First 50 IDs in A, second 50 in B
						)

# Randomise allocation of treatments by using sample function
twotreat_subjects$treatment  <- sample(twotreat_subjects$treatment) 
print(twotreat_subjects)

# Same as above but with 4 treatments of 25 subjects
four_treatments <- c("A","B","C","D")
fourtreat_subjects  <- data.frame(
						IDs = 1:100,
						treatment = rep(four_treatments, each = 25)
						)
fourtreat_subjects$treatment  <- sample(fourtreat_subjects$treatment) 
print(fourtreat_subjects)


# FVC Dataset
fvc <- read_excel("data/FVC.xlsx")
print(mean(fvc$Height))

sampled_subjects <- sample(fvc$Height, size = 10) # Sample size of 10
print(mean(sampled_subjects))

# Repeating sampled_subjects to get a distribution of means.
means  <- numeric(100) # Initialise an vector with 100 members equal to 0
for (i in 1:100) {
	means[i] <- mean(sample(fvc$Height, size = 10))
}
svglite(file="output/fvc-height-sample-histogram.svg")
hist(means)
dev.off()

# Lectures
# For a standard normal distribution N(0,1)
mu <- 0
sigma <- 1

# P(X\leq 5)=p, solve for p
print(pnorm(5,mu,sigma))

# P(X\leq c)=0.2, solve for c
print(qnorm(0.2,mu,sigma))

