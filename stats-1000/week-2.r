# This script uses the built in dataset, "mtcars" and draws histograms and box-plots using specific data.
source("lib/folder-setup.r")
library(svglite)

# Initialise data
data(mtcars)

# Histogram mtcars MPG
svglite(file="output/mtcars-mpg-histogram.svg")
hist(mtcars$mpg,main="Histogram of miles per gallon (MPG) for cars",xlab="MPG")
dev.off()

# Histogram mtcars MPG for am=0 (automatic transmission)
svglite(file="output/mtcars-mpg-am=0-histogram.svg")
hist(mtcars$mpg[mtcars$am == 0],main="Histogram of miles per gallon (MPG) for automatic transmissions",xlab="MPG")
dev.off()

# Histogram mtcars MPG for am=1 (manual transmission)
svglite(file="output/mtcars-mpg-am=1-histogram.svg")
hist(mtcars$mpg[mtcars$am == 1],main="Histogram of miles per gallon (MPG) for manual transmissions",xlab="MPG")
dev.off()

# Box-plot of MPG per transmission type
svglite(file="output/mtcars-mpg-am-boxplot.svg")
boxplot(mpg ~ am, data=mtcars,main="Boxplot of hiles per gallon (MPG) per transmission type",ylab="MPG",xlab="Transmission type")
dev.off()
