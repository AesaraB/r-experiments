load("../data/quiz3.RData")
library(lsr)

# Reps vector
## Test normalality
shapiro.test(x = reps) # Yes! Normal
## Is significant?
### Testing directly against the null hypothesis, so one-sample test.
t.test(x = reps, mu = 100)
## z-score for first element
(reps[1] - mean(reps))/sd(reps)

# Scale dataframe
## Are they the same value?
### Between subjects design, so we use independent-samples test
t.test(formula = weight ~ list, data = scale)
## Effect size
cohensD(formula = weight ~ list, data = scale, method = "pooled")

# Cholesterol
## This is a within subjects design, so a paired test.
t.test(chol$time1, chol$time2, paired=TRUE) 
cohensD(x = chol$time1, y = chol$time2, method = "paired")
