# Load libraries
library(rstatix)
library(psych)
library(lsr)

# Load data
load("../sparx.Rdata")

# Descriptive stats
mean(sparx$preDep)
sd(sparx$preDep)
hist(x = sparx$preDep)

# t-tests
## Method 1
t.test( 
	x = sparx$preDep,
	mu = 40
)

## rstatix
t_test(
	data = sparx,
	formula = preDep ~ 1,
	mu = 40
)

### Pipe operator
sparx %>% t_test(
	formula = preDep ~ 1,
	mu = 40
)

# Cohen's d
## Manual
(mean(sparx$preDep)-40 ) / sd(sparx$preDep)

## rstatix
sparx %>% cohens_d(
	formula = preDep ~ 1,
	mu = 40
)

sparx %>% cohens_d(
	formula = preDep ~ 1,
	mu = 40,
	ci = TRUE
)

# TEST YOURSELF NO. !
sparx %>% t_test(
	formula = preDep ~ 1,
	mu = 45
)
sparx %>% cohens_d(
	formula = preDep ~ 1,
	mu = 45,
	ci = TRUE
)
