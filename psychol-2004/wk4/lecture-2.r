# Load libraries
library(rstatix)
library(psych)
library(lsr)

# Load data
load("../sparx.Rdata")
sparxDiff <- sparx[sparx$Treatment == "SPARX", "prePostDiff"]
usualDiff <- sparx[sparx$Treatment == "Usual", "prePostDiff"]

# Bar plot
error.bars.by(
	x = sparx$prePostDiff,
	group = sparx$Treatment,
	by.var = TRUE,
	eyes = FALSE
)

# Welch t-test
t.test (
	formula = prePostDiff~Treatment,
	data = sparx
)

sparx %>% t_test(formula = prePostDiff~Treatment)

# TEST YOURSELF NO. 2
sparx %>% t_test(formula = preDep~Treatment)
