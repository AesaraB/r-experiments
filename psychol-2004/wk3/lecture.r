# Load libraries
library(lsr)
library(rstatix)

# Load data
load("../sparx.Rdata")
sparxDiff <- sparx[sparx$Treatment == "SPARX", "prePostDiff"]
usualDiff <- sparx[sparx$Treatment == "Usual", "prePostDiff"]

# Confidence interval
ciMean(x = sparxDiff)
mean(usualDiff)
ciMean(x = usualDiff)

# Correlation coefficient
plot(sparx$Age, sparx$prePostDiff)

## Relationship between treatment-effectiveness and age?
cor.test(sparx$Age, sparx$prePostDiff)
cor_test(data = sparx, vars = c(Age, prePostDiff))

## Correlation between pre-treatment depression and
## post-treatment depression
cor.test(sparx$preDep, sparx$postDep)
cor_test(data = sparx, vars = c(preDep, postDep))

## Correlation between prePostDiff and everything else
### More comparisons increases the probability of mistakenly finding a
### significant result when the null is true
cor_test(data = sparx, vars = prePostDiff)


# Pipes in R
sparx %>% cor_test(vars = c(Age, prePostDiff))
