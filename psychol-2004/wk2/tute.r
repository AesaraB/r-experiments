# Load data
load('drip2024.RData')

# Libraries
library(lsr)
library(psych)
library(ggpubr)
library(RcmdrMisc)

#reviewing variables in dataframe
length(drip2024)

#then we can use the str() function, to print this number of variables from drip2024

str(drip2024, list.len = 156)

# Graphs
## Histograms
hist(drip2024$Age) 
hist(drip2024$OpenMindedThinking)
hist(drip2024$OpenMindedThinking,
	main = "Open-Minded Thinking",
	xlab = "Open-Mindedness Scores",
	ylab = "Number of people",
	breaks = 14,
	col = "blue"
) 

## Boxplots
boxplot( OpenMindedThinking ~ Gender, data = drip2024 )
boxplot( OpenMindedThinking ~ Platform_use_TikTok, data = drip2024 )

## Means plot
plotMeans(drip2024$OpenMindedThinking, drip2024$Platform_use_TikTok)

### With extra arguments added
plotMeans(drip2024$OpenMindedThinking, drip2024$Platform_use_TikTok, error.bars = 'sd', 
          xlab = 'TikTok user', ylab = 'Open Mindedness', 
          main = 'Open Mindedness scores for users and non-users of TikTok', 
          connect = FALSE)

### Create a simple bar plot with error bars
ggbarplot(drip2024,
	x = "Platform_use_TikTok",
	y = "OpenMindedThinking",
	add = "mean_se"
)


##DESCRIPTIVE DATA

#example of calculating individual aspects of specific variables
mean(drip2024$Age)
sd(drip2024$Age)
min(drip2024$Age)
max(drip2024$Age)

options(max.print=10000) #this increases the number of lines R will print

#function for providing summary data for all variables
summary(drip2024)

#another function providing descriptive data for all variables in the dataframe
#describe() is contained in the psych library. Make sure this is loaded.
describe(drip2024)

#how to break down the descriptive data by certain groups
describeBy(x = drip2024, group = drip2024$Platform_use_TikTok) 

#TABLES

table( drip2024$Platform_use_TikTok, drip2024$Gender )

xtabs( ~ Platform_use_TikTok + Gender, drip2024) 

#to create an table object and then calculate proportions in each category
my_table <- table( drip2024$Platform_use_TikTok, drip2024$Gender )

prop.table(my_table)

#saving dataframe

save(drip2024 , file = "drip2024.RData") #remember this will save to your working directory

###END of SCRIPT


