# Load libraries
library(lsr)
library(psych)

# Load data
load('../sparx.Rdata')
sparxDiff <- sparx[sparx$Treatment == "SPARX", "prePostDiff"]
usualDiff <- sparx[sparx$Treatment == "Usual", "prePostDiff"]

# Histogram
hist(x = sparx$preDep)
hist(
	x = sparx$preDep,
	breaks = 20,
	main = "Pre treatment depression",
	xlab = "Depression score",
	ylab = "Frequency",
	col = "blue"
)
hist(
	x = sparxDiff,
	main = 'Depression change, SPARX group',
	xlab = "Depression score",
	ylab = "Frequency")
hist(
	x = sparxDiff,
	main = "Depression change, usual group",
	xlab = "Depression score",
	ylab = "Frequency"
)

# Boxplot
boxplot(
	x = sparx$preDep,
	main = "Pre-treatment depression",
	ylab = "Depression score"
)
boxplot(
	x = sparxDiff,
	main = 'Depression change, SPARX group',
	ylab = "Depression score"
)
boxplot(
	x = usualDiff,
	main = "Depression change, usual group",
	ylab = "Depression score"
)

# Number plots
plot(sparx$Age, sparx$prePostDiff)
plot(
	x = sparx$preDep,
	y = sparx$postDep,
	xlab = "Pre-treatment depression score",
	ylab = "Post-treatment depression score"
)


# Descriptive statistics
## Central tendencies
### Mean
mean(sparx$prePostDiff, na.rm = TRUE)
sum(sparx$prePostDiff) / length(sparx$prePostDiff)
### Median
median(sparx$prePostDiff)
quantile(sparx$prePostDiff, 0.5)
### Mode, with LSR package
modeOf(sparx$prePostDiff)
maxFreq(sparx$prePostDiff)

## Measures of spread
### Range
range(sparx$prePostDiff)
max(sparx$prePostDiff) - min(sparx$prePostDiff)
### IQR
IQR(sparx$prePostDiff)
quantile(sparx$prePostDiff, c(0.25,0.75))
### Standard Deviation
var(sparx$prePostDiff)
sd(sparx$prePostDiff)
quantile(sparx$prePostDiff, c(0.25,0.75))
### Standard Deviation
var(sparx$prePostDiff)
quantile(sparx$prePostDiff, c(0.25,0.75))

## Test yourself
mean(sparxDiff)
mean(usualDiff)
sd(sparxDiff)
mean(usualDiff)

# Frequency table
table(sparx$Sex)
table(sparx$Sex, sparx$Treatment)
## Alternate method for data entry
xtabs( formula = ~Sex + Treatment, data = sparx )

# Overview of data
summary(sparx)
describe(sparx) # From "psych" library
describeBy(x = sparx,group = sparx$Treatment) # Also from "psych"
