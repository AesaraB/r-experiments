load("../data/quiz4.RData")

# Part A
## Visualise data
boxplot(
	formula = words ~ show,
	data = d,
	ylab = "No. of words",
	xlab = "Show"
)
boxplot(
	formula = length ~ show,
	data = d,
	ylab = "No. of words",
	xlab = "Show"
)

## ANOVA
aovWords <- aov(
	formula = words ~ show,
	data = d
)
summary(aovWords)

# Part B
## Plots
plot(
	formula = length ~ words,
	data = d
)
plot(d$words, d$length)
plot(d$words, d$chars)

## Pearson's r
cor(x = d$words, y = d$length)

## Linear modelling
(lmLenChar <- lm(
	formula = words ~ length + chars,
	data = d
))
summary(lmLenChar)

lmLen <- lm(
	formula = words ~ length,
	data = d
)
summary(lmLen)
