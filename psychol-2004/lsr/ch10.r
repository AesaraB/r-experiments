library(lsr)
library(sciplot)
load("../data/lsr/afl24.Rdata")

# Confidence intervals
ciMean(x = afl$attendance)
lineplot.CI(
	data = afl,
	x.factor = year,
	xlab = "Year",
	ylab = "Average attendance",
	response = attendance,
	ci.fun = ciMean,
)

