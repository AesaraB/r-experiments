load('../data/lsr/aflsmall.Rdata')
load('../data/lsr/parenthood.Rdata')

#library(car)

# Scatter plot
plot(
	x=parenthood$dan.sleep,
	xlab="Sleep (Hours)",
	xlim=c(0,12),
	y=parenthood$dan.grump,
	ylab="Grumpiness Score",
	ylim=c(0,100)
)

pairs(
	formula = ~ dan.sleep + baby.sleep + dan.grump,
	data = parenthood
)

# Bar graph
par( mar = c( 10.1, 4.1, 4.1, 2.1) ) # Adjust graph margins
barplot(
	height = tabulate(afl.finalists),
	ylab = "Number of Final Appearances",
	names.arg = levels(afl.finalists),
	las = 2,
	xlab = "Team"
)
