# 1. Participant is shown a both sides of a card by an experimenter,
#     the card is black on one side and white on the other.
# 2. The card is placed on a surface that the participant cannot see.
# 3. The side facing up is randomised. The participant cannot witness
#     the randomisation process.
# 4. A second experimenter comes in and asks the participant
#     which side of the card is facing upwards.
binom.test(
	x = 62,
	n = 100,
	p = 0.5 # Probability of a success
)
