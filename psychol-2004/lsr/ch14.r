load("../data/lsr/clinicaltrial.Rdata")
library(gplots)

# Descriptive statistics
## Cumulative mood gain by drug and therapy
xtabs(
	formula = mood.gain ~ drug + therapy,
	data = clin.trial
)

aggregate(
	x = mood.gain ~ drug,
	data = clin.trial,
	FUN = mean
)
aggregate(
	x = mood.gain ~ drug,
	data = clin.trial,
	FUN = sd
)

## Graphs
### Plot means, error bars depict CI95
plotmeans(
	formula = mood.gain ~ drug,
	data = clin.trial,
	xlab = "Drug administered",
	ylab = "Mood gain",
	n.label = F
)


# One-way ANOVA by hand
## Group means
SSGroupMeans <- tapply(
	X = clin.trial$mood.gain,
	INDEX = clin.trial$drug,
	FUN = mean
)

## SSw
### Enumerate by drug
SSwGroupMeans <- SSGroupMeans[clin.trial$drug]
### Squared deviation
SSwSqDev <- (clin.trial$mood.gain - SSwGroupMeans)^2
### Sum-of-squares
SSw <- sum(SSwSqDev)

## SSb
### Squared deviation
SSbSqDev <- (SSGroupMeans - mean(clin.trial$mood.gain))^2
### Length of groups
SSbGroupLen <- tapply(
	X = clin.trial$mood.gain,
	INDEX = clin.trial$drug,
	FUN = length
)
### Multiply squared deviation by length of group
SSbTimesLen <- SSbSqDev * SSbGroupLen
### Sum-of-squares
SSb <- sum(SSbTimesLen)

## Mean squares
SSGroups <- length(levels(clin.trial$drug))
### MSw
DFw <- length(clin.trial$mood.gain) - SSGroups
MSw <- (SSw / DFw)
### MSb
DFb <- SSGroups - 1
MSb <- (SSb / DFb)

## F-statistic
Fstat <- (MSb / MSw)
FstatP <- pf(
	Fstat,
	df1 = DFb,
	df2 = DFw,
	lower.tail = FALSE
)

# One-way ANOVA function
aovdrug <- aov(
	formula = mood.gain ~ drug,
	data = clin.trial
)
summary(aovdrug)
