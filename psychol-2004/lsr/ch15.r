load("../data/lsr/parenthood.Rdata")

# Linear model function
(lmDanSleep <- lm(
	formula = dan.grump ~ dan.sleep,
	data = parenthood
))
summary(lmDanSleep)

## Multiple regression
(lmBothSleep <- lm(
	formula = dan.grump ~ dan.sleep + baby.sleep,
	data = parenthood
))
summary(lmBothSleep)

# Coefficient of determination R^2
X <- parenthood$dan.sleep
Y <- parenthood$dan.grump
Y.pred <- -8.94 * X + 125.97

SS.resid <- sum((Y - Y.pred)^2)
SS.tot <- sum((Y - mean(Y))^2)
