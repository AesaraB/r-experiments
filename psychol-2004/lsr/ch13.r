library(lsr)
library(psych)

load("../data/lsr/zeppo.Rdata")
load("../data/lsr/harpo.Rdata")
load("../data/lsr/chico.Rdata")
chico.long <- sortFrame(x = wideToLong(data = chico, within="time"), id)
chico$improvement <- chico$grade_test2 - chico$grade_test1
load("../data/lsr/awesome.Rdata")
load("../data/lsr/happy.Rdata")

# the t.test function is incredibly powerful, but not useful while
#  we're learning all of this.

# One-sample test
## z-test
### Grades assumed to be normally distributed
sampleMean <- mean(grades) # Grades are of psychology students
sampleLen <- length(grades)
nullMu <- 67.5 # Average grade of all students, our "null"
zSD <- 9.5 # Standard deviation of all grades
zSEM <- zSD / (sqrt(sampleLen))
zScore <- (sampleMean - nullMu) / zSEM

### Significance level covers the area of the tails.
### Since the z-score is a value on a standard normal
###  distribution (mu=0, sd=1), we can find the cumulative
###  probability (area) using pnorm

zpUpper <- pnorm(q = zScore, lower.tail = F)
zpLower <- pnorm(q = -zScore, lower.tail = T)
zpValue  <- zpLower + zpUpper # This is a two-sided test

## t-test
tSD <- sd(grades)
tSEM <- tSD / (sqrt(sampleLen))
tScore <- (sampleMean - nullMu) / tSEM

### Finding the p-value
###  t-distribution with N-1 degrees of freedom
tpUpper <- pt(q = zScore, df = sampleLen - 1, lower.tail = F)
tpLower <- pt(q = -zScore, df = sampleLen - 1, lower.tail = T)
tpValue  <- tpLower + tpUpper

### LSR package
oneSampleTTest(
	x = grades,
	mu = nullMu
	# Is there a change in grades -> is there an improvement in grades?
	#  Reports a different p value and CI95
	# one.sided = "greater"
)

# Independent-samples tests
## Student t-test
independentSamplesTTest(
	formula = grade ~ tutor, # outcome by group
	data = harpo,
	var.equal = T # Use the Student t-test
)
### This function reports a confidence interval.
###  The CI95 reported is of the difference between means
###  i.e. There is a 95% chance that this range contains
###  the true mean difference.

## Welch t-test
independentSamplesTTest(
	formula = grade ~ tutor, # outcome by group
	data = harpo
	#one.sided = "Anastasia" # The group with expected higher value.
)

# Paired-samples tests
oneSampleTTest(x = chico$improvement, mu = 0)

pairedSamplesTTest( # The lazy route.
	formula = ~ grade_test2 + grade_test1,
	data = chico
)
## Long-form test
##  Long-form data is common in repeated measures
##  So let's start getting used to using it.

pairedSamplesTTest(
	formula = grade ~ time + (id),
	data = chico.long
	#one.sided = "test2"
)

# t.test function
## One sample t-test
t.test(
	x = grades,
	mu = nullMu
)
## Student t-test
t.test(
	formula = grade ~ tutor,
	data = harpo,
	var.equal = T
)
## Welch t-test
t.test(
	formula = grade ~ tutor,
	data = harpo
)
## Paired t-test
t.test(
	x = chico$grade_test2,
	y = chico$grade_test1,
	paired = T
)

# Cohen's d
## One-sample t-test
(mean(grades) - nullMu) / sd(grades)
cohensD(x = grades, mu = nullMu)

## Independent-samples t-test
cohensD(
	formula = grade ~ tutor, # outcome by group
	data = harpo,
	method = "pooled" # Hedges' g statistic
)
### method = "raw" # Drops population estimation adjustments
### method = "x.sd" # Glass' Delta, can also use y.sd
### method = "corrected" # Adjusts for small biases
### method = "unequal" # Cohen's suggestion for the Welch t-test

## Paired t-test
cohensD(
	x = chico$grade_test2,
	y = chico$grade_test1,
	method = "paired"
)

# Testing normality
## QQ-plot
normData <- rnorm(n = 100)
hist(x = normData)
qqnorm(y = normData)

## Shapiro-Wilk test
shapiro.test(x = normData)

# Wilcoxon tests
## Two-sample
wilcox.test(
	formula = scores ~ group,
	data = awesome
	#alternative = # One/two sided test?
)

## One-sample/Paired
wilcox.test(
	x = happiness$change,
	mu = 0
)
wilcox.test(
	x = happiness$after,
	y = happiness$before,
	paired = T
)
