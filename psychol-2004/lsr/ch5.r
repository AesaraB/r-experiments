load("../data/lsr/clinicaltrial.Rdata")
load("../data/lsr/parenthood.Rdata")
load("../data/lsr/effort.Rdata")

by( data=clin.trial, INDICES=clin.trial$therapy, FUN=summary )
#describeBy( x=clin.trial, group=clin.trial$therapy )

# mood.gain by drug/therapy combination
aggregate(
	x = mood.gain ~ drug + therapy, 
	data = clin.trial, # data is in the clin.trial data frame
	FUN = mean # print out group means
)

# Correlations

## Between two variables
cor(
	x = parenthood$dan.sleep,
	y = parenthood$dan.grump
)

## Matrix for a dataframe
cor(x = parenthood)

## Spearman's R for ordinal data
cor(
	x = effort$hours,
	y = effort$grade,
	method = "spearman"
)
