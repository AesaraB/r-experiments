# Data
## LSR
library(lsr)
## "In The Night Garden"
load('../data/lsr/nightgarden.Rdata')
itng  <- data.frame(speaker, utterance)
## Likert
load('../data/lsr/likert.Rdata')
likert  <- data.frame(likert.raw)
colnames(likert) <- c("raw")
## Misc
some.data <- c(1,2,3,1,1,3,1,1,2,8,3,1,2,4,2,3,5,2)
age <- data.frame(c( 60,58,24,26,34,42,31,30,33,2,9))
colnames(age) <- c("age")
hw  <- "Hello, world!"

# Tables
## Frequency table
table(speaker)

## Cross tabulation
table(speaker, utterance)

### Cross tabulation with data frames
table(itng)
xtabs(
	formula = ~ speaker + utterance,
	data = itng
)

## Tables of proportions
prop.table(x = table(itng))
### To calculate proportions by rows/columns, use
### the "margin" argument

## Low-level tabulation
tabulate(some.data) # It's a frequency table for each number

# Transforming variables
## Likert scale centered on 0 instead of 4
likert$centred <- likert.raw - 4
### e.g. can be used to measure magnitude instead of direction
likert$strength  <- abs(likert$centred)
### or to measure direction only
likert$direction  <- sign(likert$centred)

## Cutting into groups
age$group <- cut(
	x = age$age,
	breaks = seq(from = 0, to = 60, by = 20), # Can also specify no. of breaks
	labels = c("young","adult","older")
)

# Subset of vectors
split( x = utterance, f = speaker )
# Subset of data frames
#importList( speech.by.char, ask = FALSE) # Import df vars to global env

# Subset of data frames
subset(
	   x = itng,
	   subset = speaker == "makka-pakka", # rows
	   select = utterance # columns
)

## Square brackets
### dataframe[row,column]
itng[7:10,2] # Subset can be a vector
itng[c(7,8,10),2]
itng[itng$speaker == "makka-pakka", 2] # Row can also be logical
#garden[ c("case.4", "case.5"), c("speaker", "utterance") ] # It is also possible to use row/column names

### Globbing all
#### Just leave the desired glob blank
itng[ ,2]
itng[3, ]

### Subtractive
itng[-2,] # Use negative sign to remove an element
itng[,-1]

# Sorting
## Basic sort
### Careful when working with factors, as sort works by values, not levels.
sort(some.data) # Sort numerically
sort(
	x = some.data,
	decreasing = T
)
sort(itng$utterance) # Sort alphabetically


## Data frames
sortFrame(
	x = itng,
	utterance, # Sort first by utterance
	- speaker # And then by speaker
)
sortFrame(
	x = itng,
	utterance,
	- speaker # Same, but in descending order
)

# Transposing
## Swaps the rows and columns
# Using as.data.frame bcuz transposition converts the input to a matrix.
as.data.frame(t(itng))

# Matrices
## rbind/cbind
## rowCopy/colCopy

# String manipulation
## Substrings
strtrim(
	x = hw,
	width = 5
)
substr(
	x = hw,
	start = 2,
	stop = 5
)

## Concatenation
paste("Hello", "world!")

### Concat vectors
paste(
	c("L1W1","L1W2"),
	c("L2W1","L2W2")
)
cat( # cat isn't intended for storing variables
	c("L1W1","L1W2"),
	c("L2W1","L2W2")
)

## Split strings
strsplit(
	x = hw,
	split = " " # Accepts regex by default
)

## Transformations
toupper(hw)
tolower(hw)
chartr( # Transorm individual characters
	x = hw,
	old = "lo",
	new = "wi"
)

## Search
grep( # Finds elements in a vector matching the pattern
	x = hw,
	pattern = "xyz",
)
sub( # Replace first instance of pattern
	x = hw,
	pattern = "xyz",
	replacement = "tmn"
)
gsub( # Replace all instances of pattern
	x = hw,
	pattern = "xyz",
	replacement = "tmn"
)

# Coercion
as.numeric()
as.character()
as.logical()
as.data.frame()
