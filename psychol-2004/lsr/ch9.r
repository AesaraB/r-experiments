# Binomial probability model:
#  Dice has skull on one side (success outcome)
#  Each die has a 1/6 chance of coming up skulls (probability of success)
#  Roll 20 dice (n)

# Density, probability of a given outcome
dbinom(
	x = 4,
	size = 20,
	prob = 1/6
)
dnorm()

dt()
dchisq()
df()

# Cumulative probability, finds the probability of outcomes
# smaller or equal to q (the percentile/quantile)
pbinom(
	q = 4,
	size = 20,
	prob = 1/6
)
pnorm()

pt()
pchisq()
pf()

# Finds the value equal to a percentile.
## Binomial distributions use discrete random variables, so they get rounded up.
qbinom(
	p = 0.7687492,
	size = 20,
	prob = 1/6
)
qnorm()

qt()
qchisq()
qf()

# Generates a random event using the distribution
rbinom(
	n = 100,
	size = 20,
	prob = 1/6
)
rt(n = 100)
rf()

norm.a <- rnorm(n = 1000)
chisq.a <- rchisq(n = 1000, df = 3)

## Relationship betweeen normal distribution and other dists
norm.b <- rnorm(n = 1000)
norm.c <- rnorm(n = 1000)
hist(norm.a)

### Normal distribution and chisq
chisq.b <- (norm.a)^2 + (norm.b)^2 + (norm.c)^2
hist(
	x = chisq.a,
	breaks = 50
)
hist(
	x = chisq.b,
	breaks = 50
)

### Chisq and t
norm.d <- rnorm(n = 1000)
scaled.chisq.a <- chisq.a / 3
t.a <- norm.d / sqrt(scaled.chisq.a)
hist(
	x = t.a,
	breaks = 50
)

### Chisq and F
chisq.b <- rchisq(n = 1000, df = 20)
scaled.chisq.b <- chisq.b / 20
F.a <- scaled.chisq.a / scaled.chisq.b
hist(
	x = F.a,
	breaks = 50
)
