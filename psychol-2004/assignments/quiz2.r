# Data/libs
library(psych)
library(lsr)
load('../data/quiz2.RData')


# Histograms of quantitative data
hist(
	main = "Duration of alien communication",
	x = data$contact,
	xlab = "Time (minutes)"
)
hist(
	main = "Alien body mass",
	x = data$mass,
	xlab = "Mass (kg)"
)
hist(
	main = "Alien poison sac capacity",
	x = data$poison,
	xlab = "Capacity (mL)"
)


# Bar graph of colours
barplot(
	height = tabulate(data$colour),
	names.arg = levels(data$colour)
)

# Boxplots
## Mass by colour
boxplot(
	formula = mass ~ colour,
	data = data
)

# Describing the data
describe(data)

# Poison-colour relationship
describeBy(
	x = data[,3:4],
	group = data$colour
)

boxplot(
	formula = poison ~ colour,
	data = data
)

hist(data[data$colour == "red","poison"])

# Are aliens friendly?
data$isFriendly  <- data$contact > 40
table(data$isFriendly)
## Cross-tabulation with colour
(friendlyByColour  <- xtabs( # Delimit variable assignment to print the output
	formula = ~ isFriendly + colour,
	data = data
))
prop.table(
	x = friendlyByColour,
	margin = 1
)
